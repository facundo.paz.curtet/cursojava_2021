package modulo2;

public class Ejercicio2 {

	public static void main(String[] args) {
		byte 		bmin = -128;
		byte 		bmax = 127;
		short 	    smin = -32768;
		short 	    smax = 32767;
		int 		imax = -2147483648;
		int 		imin = 2147483647;
		long 		lmin = -2^(63);
		long 		lmax =  2^(63)-1;

		System.out.println("tipo\tminimo\tmaximo");
		System.out.println("....\t......\t......");
		System.out.println("\nbyte\t" + bmin + "\t" + bmax);
		System.out.println("\nshort\t" + smin + "\t" + smax);
		System.out.println("\nint\t" + imin + "\t" + imax);
		System.out.println("\nlong\t" + lmin + "\t" + lmax);

	}

}
