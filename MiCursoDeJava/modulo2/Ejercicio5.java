package modulo2;
/*
5-	Cual de las siguientes líneas dan errores de compilación y para esos casos cubrirlos con el casteo correspondiente

I = s;    
b=s;
l=i;
b=i;
s=i;

 */
public class Ejercicio5 {

	public static void main(String[] args) {
		
		byte 		bmin = -128;
		byte 		bmax = 127;
		short 	    smin = -32768;
		short 	    smax =32767;
		int 		imax = 2147483647;
		int 		imin = -2147483648;
		long 		lmin = -2^(63);
		long 		lmax = 2^(63)-1;

		int  i = smax; 
		byte b = (byte)smax; 
		long l = imax;
		byte b1 = (byte)imax; 
		short s= (short)imax; 

		
		
	}

}
