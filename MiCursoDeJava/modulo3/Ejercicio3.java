package modulo3;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un mes");
		String nombre = scan.nextLine();
		
		if (nombre.equals("enero")) {
			System.out.println("El mes es enero y tiene 31 dias");
		}
		else if (nombre.equals("febrero")) {
			System.out.println("El mes de febrero tiene 28 dias");
		}
		else if (nombre.equals("marzo")) {
			System.out.println("El mes de marzo tiene 31 dias");
		}
		else if (nombre.equals("abril")) {
			System.out.println("El mes de abril tiene 30 dias");
		}
		else if (nombre.equals("mayo")) {
			System.out.println("El mes de mayo tiene 31 dias");
		}
		else if (nombre.equals("junio")) {
			System.out.println("El mes de junio tiene 30 dias");
		}
		else if (nombre.equals("julio")) {
			System.out.println("El mes de julio tiene 31 dias");
		}
		else if (nombre.equals("agosto")) {
			System.out.println("El mes de agosto tiene 31 dias");
		}
		else if (nombre.equals("septiembre")) {
			System.out.println("El mes de septiembre tiene 30 dias");
		}
		else if (nombre.equals("octubre")) {
			System.out.println("El mes de octubre tiene 31 dias");
		}
		else if (nombre.equals("noviembre")) {
			System.out.println("El mes de noviembre tiene 30 dias");
		}
		else if (nombre.equals("diciembre")) {
			System.out.println("El mes de dicimbre tiene 31 dias");
		}
		

	}

}
