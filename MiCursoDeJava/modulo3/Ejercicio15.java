package modulo3;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese la clase del auto");
		char clase = scan.next().charAt(0);
		
		switch (clase) {
		case 'a': case 'A' : System.out.println("La clase A tiene 4 ruedas y un motor");
		break;
		case 'b': case 'B': System.out.println("La clase B tiene 4 ruedas, un motor, cierre centralizado y aire ");
		break;
		case 'c': case 'C': System.out.println("La clase C tiene 4 ruedas,  un motor, cierre centralizado, aire, airbag. ");
		break;
		default : System.out.println("La clase que ingreso no existe");
		break;
		
		}
	}

}
