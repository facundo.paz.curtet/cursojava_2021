package modulo3;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el numero que quiera saber la tabla");
		int numero = scan.nextInt();
		
		for (int i=0;i<=10;i++ ) {
			int resultado = numero * i;
			System.out.println("El " + numero +" multiplicado por "+ i + " es igual a "+ resultado);
		}
		

	}

}
