package modulo3;

import java.util.Scanner;

public class Ejercicio6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un numero del 0 al 12");
		int numero = scan.nextInt();
		
		if (numero == 0 ) {
			System.out.println("Jardin de infantes");
		}
		else if (numero >= 1 && numero <= 6){
			System.out.println("Primaria");
		}
		else if (numero >= 7 && numero <= 12 ) {
			System.out.println("Secundaria");
		}

	}

}
