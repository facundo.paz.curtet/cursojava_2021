package modulo3;

import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese el puesto(en numeros)");
		int puesto = scan.nextInt();
		
		if (puesto == 1) {
			System.out.println("El primero obtiene la medalla de oro");
		}
		else if (puesto == 2) {
			System.out.println("El segundo obtiene la medalla de plata");
		}
		else if (puesto == 3) {
			System.out.println("El tercero obtiene la medalla de bronce.");
		}
		else {
			System.out.println("Segui participando");
		}
		

	}

}
