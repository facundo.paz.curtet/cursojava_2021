package modulo3;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Ingrese un numero");
		Scanner scan = new Scanner (System.in);
		int numero = scan.nextInt();
		
		if (numero%2==0) {
			System.out.println("El numero " + numero + " es par");
		}
		else {
			System.out.println("El numero "+ numero + " es impar");
		}
	}

}
